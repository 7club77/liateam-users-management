<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $user = App\User::create(['name' => 'admin', 'email' => 'admin@test.com', 'password' => app('hash')->make(App\User::ADMIN_PASS)]);
        $user->assignRole('admin');
        $user->givePermissionTo('edit user role');

    }
}
