<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
 */

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function () use ($router) {
    $router->get('/users', 'User\UserController@index');
    $router->post('/users', 'User\UserController@store');
    $router->put('/users', 'User\UserController@update');
    $router->put('/users/edit/{user_id}', 'User\UserController@edit');
    $router->post('/roles', 'Role\RoleController@store');
    $router->post('/permissions', 'Permission\PermissionController@store');

});