<?php

namespace App\Http\Controllers\Permission;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionController extends Controller {
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['only' => ['store']]);
        $this->middleware('role:admin', ['only' => ['store']]);
    }

    public function store(Request $request) {
        if (!$request->has('role')) {
            return $this->errorResponse("role attribute doesn't exist", 400);
        } else {
            $role = Role::where('name', $request->role)->first();
            if ($role === null) {
                return $this->errorResponse("role doesn't exist", 400);
            }

        }
        if ($request->has('permission')) {
            Permission::create(['name' => $request->permission]);
            $role->givePermissionTo($request->permission);

        } else {
            return $this->errorResponse("permission attribute doesn't exist", 400);
        }
        return $this->successResponse(['status' => 1], 201);
    }

}
