<?php

namespace App\Http\Controllers\Role;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RoleController extends Controller {
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('auth:api', ['only' => ['store']]);
        $this->middleware('role:admin', ['only' => ['store']]);
    }

    public function store(Request $request) {
        if ($request->has('role')) {
            $role = Role::create(['name' => $request->role]);
        } else {
            return $this->errorResponse("role attribute doesn't exist", 400);
        }
        return $this->successResponse($role, 201);
    }

}
