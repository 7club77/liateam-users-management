<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Traits\ApiResponser;
use App\User;
use Illuminate\Http\Request;
use Validator;

class UserController extends Controller {
    use ApiResponser;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct() {
        $this->middleware('client.credentials', ['only' => ['store']]);
        $this->middleware('auth:api', ['except' => ['store']]);
        $this->middleware('role:admin', ['only' => ['index']]);
        $this->middleware('permission:edit user role', ['only' => ['edit', 'index']]);

    }

    public function index(Request $request) {
        $users = User::all();
        return $this->showAll($users);
    }

    public function store(Request $request) {
        $rules = [
            'name' => 'required|unique:users',
            'email' => 'required|unique:users',
            'password' => 'required|min:6|confirmed',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            $errors = $validator->errors();
            foreach ($errors->get('name') as $message) {
                return $this->errorResponse($message, 400);
            }
            foreach ($errors->get('email') as $message) {
                return $this->errorResponse('ایمیل تکراری است.', 400);
            }
        }

        $data = $request->all();
        $data['password'] = app('hash')->make($request->password);
        $user = User::create($data);
        $user->assignRole('regular');
        $token = $user->createToken('user');
        $user['access_token'] = $token->accessToken;

        return $this->showOne($user, 201);

    }

    public function update(Request $request) {
        $user = $request->user();
        $this->authorize('update', $user);
        $rules = [
            'name' => 'required|unique:users',
            'email' => 'email|unique:users',
            'password' => 'min:6|confirmed',
        ];

        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return $this->errorResponse($validator->errors(), 400);
        }
        if ($request->has('name')) {
            $user->name = $request->name;
        }
        if ($request->has('email')) {
            $user->email = $request->email;
        }
        if ($request->has('password')) {
            $user->password = app('hash')->make($request->password);
        }

        $user->save();
        return $this->showOne($user, 200);
    }

    public function edit(Request $request, $user_id) {
        $user = User::findOrFail($user_id);
        if ($request->has('role')) {
            $user->assignRole($request->role);
        }

        if ($request->has('permission')) {
            $user->givePermissionTo($request->permission);
        }
        return $this->showOne($user, 200);
    }
}
