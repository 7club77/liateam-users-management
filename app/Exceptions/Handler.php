<?php

namespace App\Exceptions;

use App\Traits\ApiResponser;
use Exception;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Validation\ValidationException;
use Laravel\Lumen\Exceptions\Handler as ExceptionHandler;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Handler extends ExceptionHandler {
    use ApiResponser;
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        \Illuminate\Auth\Access\AuthorizationException::class,
        \Illuminate\Auth\AuthenticationException::class,
        \Symfony\Component\HttpKernel\Exception\HttpException::class,
        \Illuminate\Database\Eloquent\ModelNotFoundException::class,
        \Illuminate\Session\TokenMismatchException::class,
        \Illuminate\Validation\ValidationException::class,

    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'password',
        'password_confirmation',
    ];

    /**
     * Report or log an exception.
     *
     * This is a great spot to send exceptions to Sentry, Bugsnag, etc.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function report(Exception $exception) {
        parent::report($exception);
    }

    protected function unauthenticated($request, AuthenticationException $exception) {
        return $this->errorResponse('خطای احراز هویت', 401);

    }

    protected function convertValidationExceptionToResponse(ValidationException $e, $request) {

        $errors = $e->validator->errors()->getMessages();

        return $this->errorResponse($errors, 422);
    }

    /**
     * Render an exception into an HTTP response.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Exception  $exception
     * @return \Illuminate\Http\Response
     */
    public function render($request, Exception $exception) {
        dd($exception);
        if ($exception instanceof ValidationException) {
            return $this->convertValidationExceptionToResponse($exception, $request);
        }
        if ($exception instanceof ModelNotFoundException) {
            return $this->errorResponse($request, $exception);
            # code...
        }
        if ($exception instanceof AuthenticationException) {
            return $this->unauthenticated($request, $exception);
            # code...
        }
        if ($exception instanceof AuthorizationException) {
            return $this->errorResponse($exception->getMessage(), 403);
            # code...
        }
        if ($exception instanceof MethodNotAllowedHttpException) {
            return $this->errorResponse('مِتُد ارسالی برای این درخواست معتبر نیست', 405);
            # code...
        }
        if ($exception instanceof HttpException) {

            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
            # code...
        }
        if ($exception instanceof QueryException) {
            return $this->errorResponse($exception->getMessage(), $exception->getStatusCode());
            # code...
        }
        if ($exception instanceof NotFoundHttpException) {
            if ($exception->errorInfo[1] == 1451) {
                return $this->errorResponse('Cannot remove this resource', 409);
            }
        }

        // return $this->errorResponse('خطای غیرمنتظره', 500);
        return parent::render($request, $exception);
    }

}
