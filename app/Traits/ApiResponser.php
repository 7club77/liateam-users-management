<?php

namespace App\Traits;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Validator;

trait ApiResponser {
    private function successResponse($data, $code) {
        return response()->json($data, $code, [], JSON_NUMERIC_CHECK);
    }

    protected function errorResponse($message, $code) {
        return response()->json(['error' => $message, 'code' => $code], $code);
    }

    protected function showAll(Collection $collection, $code = 200) {
        if ($collection->isEmpty()) {
            return $this->successResponse(['data' => $collection], $code);
        }

        $collection = $this->filterData($collection);
        $collection = $this->sortData($collection);
        $collection = $this->paginate($collection);
        $collection = $this->cacheResponse($collection);

        return $this->successResponse($collection, $code);
    }

    protected function showOne(Model $instance, $code = 200) {
        return $this->successResponse($instance, $code);
    }

    protected function filterData(Collection $collection) {
        foreach (app('request')->query() as $query => $value) {
            $attribute = $query;
            if (isset($attribute, $value)) {
                if ($value === 'true' || $value === 'TRUE') {
                    $value = 1;
                }
                if ($value === 'false' || $value === 'FALSE') {
                    $value = 0;
                }
                if ($query === 'sort_by' || $query === 'sort_order' || $query == 'per_page' || $query == 'page') {
                    return $collection;
                } else {
                    $collection = $collection->where($attribute, $value);
                }

            }
        }
        return $collection;
    }

    protected function sortData(Collection $collection) {

        if (app('request')->has('sort_by')) {
            $attribute = app('request')->sort_by;
            $collection = $collection->sortBy->{$attribute};
        }
        if (app('request')->has('sort_order')) {
            if (app('request')->sort_order === "descend") {
                $attribute = app('request')->sort_by;
                $collection = $collection->sortByDesc->{$attribute};
            } else if (app('request')->sort_order === "ascend") {
                $attribute = app('request')->sort_by;
                $collection = $collection->sortBy->{$attribute};
            }
        }

        return $collection;
    }

    protected function paginate(Collection $collection) {
        $rules = [
            'per_page' => 'integer|min:2|max:500',
        ];
        Validator::validate(app('request')->all(), $rules);
        $page = LengthAwarePaginator::resolveCurrentPage();
        $perPage = 10;
        if (app('request')->has('per_page')) {
            $perPage = (int) app('request')->per_page;
        }
        $results = $collection->slice(($page - 1) * $perPage, $perPage)->values();
        $paginated = new LengthAwarePaginator($results, $collection->count(), $perPage, $page, [
            'path' => LengthAwarePaginator::resolveCurrentPath(),
        ]);
        $paginated->appends(app('request')->all());
        return $paginated;
    }

    protected function cacheResponse($data) {
        $url = app('request')->url();
        $queryParams = app('request')->query();

        ksort($queryParams);

        $queryString = http_build_query($queryParams);

        $fullUrl = "{url}?{$queryString}";

        return Cache::remember($fullUrl, 30 / 60, function () use ($data) {
            return $data;
        });
    }

}